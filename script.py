import urllib.request
import time
import os.path
from os import path

url_pc = "http://n9e5v4d8.ssl.hwcdn.net/repos/weeklyRivensPC.json"
url_ps4 = "http://n9e5v4d8.ssl.hwcdn.net/repos/weeklyRivensPC.json"
url_xb1 = "http://n9e5v4d8.ssl.hwcdn.net/repos/weeklyRivensPC.json"
url_switch = "http://n9e5v4d8.ssl.hwcdn.net/repos/weeklyRivensPC.json"


def retrieveAll():
	#"%Y%m%d-%H%M%S"
	date = time.strftime("%Y_%m_%d")
	out_pc = "PC/weeklyRivensPC_"+date+".json"
	out_ps4 = "PS4/weeklyRivensPC_"+date+".json"
	out_xb1 = "XB1/weeklyRivensPC_"+date+".json"
	out_switch = "SWITCH/weeklyRivensPC_"+date+".json"
	
	if(path.exists(out_pc)):
		print(out_pc+" already exists.")
	else:
		urllib.request.urlretrieve(url_pc, out_pc)
	if(path.exists(out_ps4)):
		print(out_ps4+" already exists.")
	else:
		urllib.request.urlretrieve(url_ps4, out_ps4)
	if(path.exists(out_xb1)):
		print(out_xb1+" already exists.")
	else:
		urllib.request.urlretrieve(url_xb1, out_xb1)
	if(path.exists(out_switch)):
		print(out_switch+" already exists.")
	else:
		urllib.request.urlretrieve(url_switch, out_switch)

retrieveAll()